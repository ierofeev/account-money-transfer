**Account money transfer app with JPA implementation **

---

## RESTful API for money transfers between accounts.

1. JAX-RS
2. H2 in memory database
3. Jersey / Jetty
4. Guice
5. HikariCP as connection pool
6. Hibernate-entitymanager

Application starts a jetty server on localhost port 9080 (http://localhost:9080/) H2 database initialized with ACCOUNT table.
---

## API
---

Create account

Request:
POST: http://localhost:9080/money-transfer/accounts
```
{
	"userName": "Max",
	"amount":"1000",
	"currencyCode":"RUR"
}
```

Response:
```
{
    "id": 1,
    "userName": "Max",
	"amount": 1000,
	"currencyCode": "RUR",
    "version": 0
}
```
---

Get accounts:

Request:
GET: http://localhost:9080/money-transfer/accounts/1

Response:
```
{
    "id": 1,
    "userName": "Ivan",
	"amount": 10650,
    "currencyCode": "RUR",
    "version": 1
}
```

Request:
GET: http://localhost:9080/money-transfer/accounts

Response:
```
[
    {
        "amount": 950,
        "currencyCode": "RUR",
        "id": 1,
        "userName": "Ivan",
        "version": 1
    },
    {
        "amount": 1050,
        "currencyCode": "RUR",
        "id": 2,
        "userName": "Max",
        "version": 1
    }
]
```
---

Update account

Request:
PUT: http://localhost:9080/money-transfer/accounts
```
{
    "amount": 10650,
    "currencyCode": "RUR",
    "id": 1,
    "userName": "Ivan",
    "version": 0
}
```
Response:
```
{
    "amount": 10650,
    "currencyCode": "RUR",
    "id": 1,
    "userName": "Ivan",
    "version": 1
}
```
---
Delete account

Request:
DELETE: http://localhost:9080/money-transfer/accounts/1

