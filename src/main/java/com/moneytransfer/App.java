package com.moneytransfer;

import com.google.inject.servlet.GuiceFilter;
import com.moneytransfer.infrastructure.guice.InitializeGuiceModulesContextListener;
import com.moneytransfer.infrastructure.h2.H2Initializer;
import com.moneytransfer.utils.DbUtils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class App {

    private static Server server;
    private static final int PORT = 9080;

    public static void main(String[] args) throws Exception {
        new App().run();
    }

    private void run() throws Exception {
        createServer();
        bindGuiceContextToServer();

        DbUtils.loadH2Config();
        H2Initializer.initDb();

        startServer();
        waitForServerToFinnish();
    }

    void bindGuiceContextToServer() {
        ServletContextHandler context = createRootContext();
        serveGuiceContext(context);
    }

    private void serveGuiceContext(ServletContextHandler context) {
        bindGuiceContextAndFilter(context);
        addDefaultServletToContext(context);
    }

    private void addDefaultServletToContext(ServletContextHandler context) {
        /*
         * Jetty requires some servlet to be bound to the path, otherwise request is just skipped. This prevents Guice
         * from handling the request, because it is done through filter.
         */
        context.addServlet(DefaultServlet.class, "/");
    }

    private void bindGuiceContextAndFilter(ServletContextHandler context) {
        context.addEventListener(new InitializeGuiceModulesContextListener());
        context.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));
    }

    private ServletContextHandler createRootContext() {
        ServletContextHandler context = new ServletContextHandler();
        context.setContextPath("");
        server.setHandler(context);
        return context;
    }

    private void waitForServerToFinnish() throws InterruptedException {
        server.join();
    }

    void startServer() throws Exception {
        server.start();
    }

    void stopServer() throws Exception {
        server.stop();
    }

    void createServer() {
        server = new Server(PORT);
    }
}
