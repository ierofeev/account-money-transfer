package com.moneytransfer.account;

import com.moneytransfer.utils.EntityManagerUtil;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Logger;

public class ServiceImpl implements Service {

    private Logger logger = Logger.getLogger(ServiceImpl.class.toString());

    @Override
    public Account createAccount(Account account) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(account);
        em.getTransaction().commit();

        return account;
    }

    @Override
    public Account updateAccount(Account account) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(account);
        em.getTransaction().commit();

        return account;
    }

    @Override
    public void deleteAccount(int id) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        Account account = em.find(Account.class, id);
        if (account != null) {
            em.remove(account);
        }
        em.getTransaction().commit();
    }

    @Override
    public Account getAccount(int id) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        return em.find(Account.class, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Account> getAccounts() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        Query query = em.createNamedQuery(Account.FIND_ALL);

        return query.getResultList();
    }
}
