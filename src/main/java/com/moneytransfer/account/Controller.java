package com.moneytransfer.account;

import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/money-transfer/accounts")
@RequestScoped
public class Controller {

    private Service service;

    @Inject
    public Controller(Service service) {
        this.service = service;
    }

    /**
     * Handler for GET request
     * with accountId as a parameter of request
     *
     * @param accountId unic id in database
     * @return Account by accountId
     */
    @GET
    @Path("/{accountId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Account getAccount(@PathParam("accountId") int accountId) {
        return service.getAccount(accountId);
    }

    /**
     * Handler for GET request without any parameters
     *
     * @return List of all existing Accounts
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Account> getAccounts() {
        return service.getAccounts();
    }

    /**
     * Handler for POST request with name, amount, currency as parameters
     * Creates a new Account and insert
     * Account data with unique AccountId in H2 in memory DB
     *
     * @param account - account
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Account createAccount(Account account) {
        return service.createAccount(account);
    }

    /**
     * Handler for PUT request
     * to update existing account
     *
     * @param account - account
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Account updateAccount(Account account) {
        return service.updateAccount(account);
    }

    /**
     * Handler for DELETE request
     * to delete existing account
     *
     * @param accountId account id of existing Account
     */
    @DELETE
    @Path("/{accountId}")
    public void deleteAccount(@PathParam("accountId") int accountId) {
        service.deleteAccount(accountId);
    }
}
