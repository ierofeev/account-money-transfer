package com.moneytransfer.account;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@NamedQueries({
        @NamedQuery(name = Account.FIND_ALL, query = "SELECT e FROM Account e order by e.id "),
})
@Entity
@Table(name = "T_ACCOUNT")
public class Account implements Serializable {

    public static final String FIND_ALL = "account.Account.find_all";

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "VERSION")
    @Version
    private int version;

    @Column(name = "NAME")
    private String userName;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "CURRENCY_CODE")
    private String currencyCode;

    public Account(String userName, BigDecimal amount, String currencyCode) {
        this.userName = userName;
        this.amount = amount;
        this.currencyCode = currencyCode;
    }
}
