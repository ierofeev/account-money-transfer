package com.moneytransfer.account;

import java.util.List;

public interface Service {

    /**
     * Create new account
     *
     * @param account
     * @return created account
     */
    Account createAccount(Account account);

    /**
     * Update current account
     *
     * @param account
     * @return updated account
     */
    Account updateAccount(Account account);

    /**
     * Delete account
     *
     * @param accountId
     */
    void deleteAccount(int accountId);

    /**
     * Fetch account
     *
     * @param accountId
     * @return account
     */
    Account getAccount(int accountId);

    /**
     * Fetch accounts
     *
     * @return List<Account>
     */
    List<Account> getAccounts();
}
