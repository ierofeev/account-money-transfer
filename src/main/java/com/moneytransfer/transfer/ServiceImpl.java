package com.moneytransfer.transfer;

import com.google.inject.Inject;
import com.moneytransfer.account.Account;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceImpl implements Service {

    private static Logger logger = Logger.getLogger(ServiceImpl.class.toString());

    private com.moneytransfer.account.Service accountService;

    @Inject
    public ServiceImpl(com.moneytransfer.account.Service accountService) {
        this.accountService = accountService;
    }

    public void transferAmount(Dto dto) {
        Account from = accountService.getAccount(dto.getFrom());
        Account to = accountService.getAccount(dto.getTo());

        if (isTransferValid(from, to, dto.getAmount())) {
            from.setAmount(from.getAmount().subtract(dto.getAmount()));
            to.setAmount(to.getAmount().add(dto.getAmount()));

            accountService.updateAccount(from);
            accountService.updateAccount(to);
        }
    }

    private boolean isTransferValid(Account fromAccount, Account toAccount, BigDecimal amount) {
        if (fromAccount != null && toAccount != null) {
            BigDecimal fromAmount = fromAccount.getAmount();
            if (fromAmount.subtract(amount).compareTo(BigDecimal.ZERO) > -1) {
                return true;
            } else {
                logger.log(Level.WARNING, "Exception during transfer amount, amount is too big: from " +
                        fromAccount.getId() + " to " + toAccount.getId() + " : amount " + amount.doubleValue());
                throw new RuntimeException("Exception during transfer amount, amount is too big");
            }
        } else {
            logger.log(Level.WARNING, "Exception during transfer amount, account can't be null");
            throw new RuntimeException("Exception during transfer amount, account can't be empty");
        }
    }
}
