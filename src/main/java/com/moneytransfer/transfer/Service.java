package com.moneytransfer.transfer;

public interface Service {

    /**
     * Transfer amount from one account to another
     *
     * @param dto - transfer object
     */
    void transferAmount(Dto dto);
}
