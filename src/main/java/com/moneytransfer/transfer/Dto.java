package com.moneytransfer.transfer;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class Dto {
    /**
     * Write-off account
     */
    private int from;

    /**
     * Enrollment account
     */
    private int to;


    private BigDecimal amount;
}
