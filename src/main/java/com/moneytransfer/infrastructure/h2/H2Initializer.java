package com.moneytransfer.infrastructure.h2;

import com.moneytransfer.utils.DbUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class H2Initializer {

    private static Logger logger = Logger.getLogger(H2Initializer.class.toString());

    public static void initDb() {
        try (Connection connection = DbUtils.getConnection();
             Statement statement = connection.createStatement()) {

            connection.setAutoCommit(false);

            statement.execute("CREATE SEQUENCE IF NOT EXISTS ID_SEQ START WITH 1 INCREMENT BY 1");
            statement.execute("CREATE SEQUENCE IF NOT EXISTS REC_SEQ START WITH 1 INCREMENT BY 1");
            statement.execute("CREATE TABLE T_ACCOUNT(ID INTEGER IDENTITY NOT NULL, " +
                    "VERSION INTEGER NOT NULL, NAME VARCHAR(255), " +
                    "AMOUNT NUMBER DEFAULT 0, CURRENCY_CODE VARCHAR(3))");

            connection.commit();
        } catch (SQLException  e) {
            logger.log(Level.WARNING, "Exception during initializing database", e);
            throw new RuntimeException("Exception during initializing database");
        }
    }
}
