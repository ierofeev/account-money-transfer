package com.moneytransfer.infrastructure.guice;

import com.google.inject.servlet.ServletModule;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

import java.util.HashSet;
import java.util.Set;

class BindJerseyResourcesModule extends ServletModule {

    @Override
    protected void configureServlets() {
        bindResources();
        serveBoundResources();
    }

    private void bindResources() {
        lookupResources().forEach(this::bind);
    }

    private Set<Class<?>> lookupResources() {
        Set<Class<?>> result = new HashSet<>();
        PackagesResourceConfig accountResourceConfig = new PackagesResourceConfig("com.moneytransfer.account");
        PackagesResourceConfig transferResourceConfig = new PackagesResourceConfig("com.moneytransfer.transfer");
        result.addAll(accountResourceConfig.getClasses());
        result.addAll(transferResourceConfig.getClasses());

        return result;
    }

    private void serveBoundResources() {
        serve("/*").with(GuiceContainer.class);
    }
}