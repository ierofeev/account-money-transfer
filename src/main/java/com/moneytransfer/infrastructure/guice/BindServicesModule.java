package com.moneytransfer.infrastructure.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

class BindServicesModule extends AbstractModule {

    @Override
    protected void configure() {
        bindAccountModule();
        bindTransferModule();
    }

    private void bindAccountModule() {
        bind(com.moneytransfer.account.Service.class).to(com.moneytransfer.account.ServiceImpl.class).in(Singleton.class);
    }

    private void bindTransferModule() {
        bind(com.moneytransfer.transfer.Service.class).to(com.moneytransfer.transfer.ServiceImpl.class).in(Singleton.class);
    }

}
