package com.moneytransfer.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EntityManagerUtil {
    private static volatile EntityManager entityManager;

    public static EntityManager getEntityManager() {
        EntityManager localEntityManager = entityManager;
        if (localEntityManager == null) {
            synchronized (EntityManagerUtil.class) {
                localEntityManager = entityManager;
                if (localEntityManager == null) {
                    EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("jpa-money-transfer");
                    entityManager = emFactory.createEntityManager();
                }
            }
        }

        return entityManager;
    }
}
