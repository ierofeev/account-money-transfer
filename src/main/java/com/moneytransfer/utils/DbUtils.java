package com.moneytransfer.utils;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbUtils {
    private static Logger logger = Logger.getLogger(DbUtils.class.toString());

    private static Properties properties = new Properties();
    private static HikariDataSource dataSource;

    public static void loadH2Config() {
        try (InputStream inputStream =
                     Thread.currentThread().getContextClassLoader().getResourceAsStream("h2.properties")) {

            properties.load(inputStream);

            String h2_driver = getStringProperty("h2_driver");
            String h2_connection_url = getStringProperty("h2_connection_url");
            HikariConfig config = new HikariConfig();

            config.addDataSourceProperty("h2_driver", h2_driver);
            config.addDataSourceProperty("h2_connection_url", h2_connection_url);
            config.addDataSourceProperty("URL", getStringProperty("h2_connection_url"));
            config.addDataSourceProperty("user", getStringProperty("h2_user"));
            config.addDataSourceProperty("password", getStringProperty("h2_password"));
            config.setDriverClassName(h2_driver);
            config.setConnectionTestQuery("SELECT 1");
            config.setJdbcUrl(h2_connection_url);
            config.setTransactionIsolation("TRANSACTION_REPEATABLE_READ");
            dataSource = new HikariDataSource(config);
            dataSource.setMaximumPoolSize(5);

        } catch (Exception e) {
            logger.log(Level.WARNING, "Exception during loading property file", e);
            throw new RuntimeException("Exception during loading property file");
        }
    }

    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    private static String getStringProperty(String key) {
        return properties.getProperty(key);
    }
}
