package com.moneytransfer;

import com.moneytransfer.account.Account;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @InjectMocks
    private com.moneytransfer.account.ServiceImpl accountService;

    private static App app;

    private static final String ACCOUNTS_URL = "http://localhost:9080/money-transfer/accounts";

    @BeforeClass
    public static void initApp() throws Exception {
        app = TestUtils.initApp();
        app.startServer();
    }

    @Test
    public void getAllAccountsTest() throws Exception {
        HttpURLConnection http =
                (HttpURLConnection)new URL(ACCOUNTS_URL).openConnection();
        http.connect();
        assertEquals(http.getResponseCode(), HttpStatus.OK_200);
    }

    @Test
    public void getAllAccountsMockTest() {
        List<Account> accounts = new ArrayList<>();
        assertEquals(accounts.size(), accountService.getAccounts().size());
    }

    @Test
    public void getAccountTest() throws IOException {
        String accountString = "{\"userName\":\"John\", \"amount\":\"1000\", \"currencyCode\":\"RUR\"}";
        HttpURLConnection httpCreate = prepareCreateConnection(accountString);
        assertEquals(HttpStatus.OK_200, httpCreate.getResponseCode());
        List<Account> accounts = accountService.getAccounts();
        Account account = accounts.stream()
                .filter(acc -> "John".equals(acc.getUserName()))
                .findAny()
                .orElseThrow(RuntimeException::new);
        assertEquals("John", account.getUserName());
        assertEquals(BigDecimal.valueOf(1000), account.getAmount());
        assertEquals("RUR", account.getCurrencyCode());
    }

    @Test
    public void getAccountMockTest() {
        assertNull(accountService.getAccount(12));
    }

    @Test
    public void updateAccountTest() throws IOException {
        HttpURLConnection httpCreate = prepareCreateConnection();
        assertEquals(HttpStatus.OK_200, httpCreate.getResponseCode());
        Account account = accountService.getAccount(1);

        account.setAmount(BigDecimal.TEN);
        Account updatedAccount = accountService.updateAccount(account);
        assertEquals(BigDecimal.TEN, updatedAccount.getAmount());
    }

    @Test
    public void createAccountTest() throws IOException {
        Account account = new Account();
        account.setAmount(BigDecimal.TEN);
        account.setUserName("Max");
        account.setCurrencyCode("USD");

        Account createdAccount = accountService.createAccount(account);
        assertTrue(createdAccount.getId() > 0);
        assertEquals(0, createdAccount.getVersion());
    }

    @AfterClass
    public static void stopServer() throws Exception {
        app.stopServer();
    }

    private HttpURLConnection prepareCreateConnection() throws IOException {
        String accountString = "{\"userName\":\"Ivan\", \"amount\":\"1000\", \"currencyCode\":\"RUR\"}";
        return prepareCreateConnection(accountString);
    }

    private HttpURLConnection prepareCreateConnection(String query) throws IOException {
        HttpURLConnection httpCreate =
                (HttpURLConnection)new URL(ACCOUNTS_URL).openConnection();
        httpCreate.setRequestMethod("POST");
        httpCreate.setRequestProperty("Content-Type", "application/json");
        httpCreate.setRequestProperty("Accept", "application/json");
        httpCreate.setDoOutput(true);
        try(DataOutputStream dataOutputStream =
                    new DataOutputStream(httpCreate.getOutputStream())) {
            byte[] postData = query.getBytes(StandardCharsets.UTF_8);
            dataOutputStream.write(postData, 0, postData.length);
        }

        return httpCreate;
    }

    private HttpURLConnection prepareUpdateConnection(Account account) throws IOException {
        String accountId = String.valueOf(account.getId());
        String updatedAccountString = "{\"accountId\":\"" + accountId + "\", \"recordedVersion\":\"1\", " +
                "\"userName\":\"Ivan\", \"amount\":\"11500\", \"currencyCode\":\"USD\"}";
        HttpURLConnection httpUpdate =
                (HttpURLConnection)new URL(ACCOUNTS_URL).openConnection();
        httpUpdate.setRequestMethod("PUT");
        httpUpdate.setRequestProperty("Content-Type", "application/json");
        httpUpdate.setRequestProperty("Accept", "application/json");
        httpUpdate.setDoOutput(true);
        try(DataOutputStream dataOutputStream =
                    new DataOutputStream(httpUpdate.getOutputStream())) {
            byte[] postData = updatedAccountString.getBytes(StandardCharsets.UTF_8);
            dataOutputStream.write(postData, 0, postData.length);
        }

        return httpUpdate;
    }
}
