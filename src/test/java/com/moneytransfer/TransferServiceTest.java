package com.moneytransfer;

import com.moneytransfer.account.Account;
import com.moneytransfer.transfer.Dto;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

    @Mock
    private com.moneytransfer.account.Service accountService;

    @InjectMocks
    private com.moneytransfer.transfer.ServiceImpl transferService;

    private static App app;

    @BeforeClass
    public static void initApp() throws Exception {
        app = TestUtils.initApp();
        app.startServer();
    }

    @Test
    public void transferAmountTest() {
        Account from = new Account("Dmitriy", BigDecimal.valueOf(1000), "RUR");
        from.setId(1);
        when(accountService.getAccount(from.getId())).thenReturn(from);

        Account to = new Account("Yakov", BigDecimal.valueOf(1000), "RUR");
        to.setId(2);
        when(accountService.getAccount(to.getId())).thenReturn(to);

        Dto dto = new Dto();
        dto.setFrom(from.getId());
        dto.setTo(to.getId());
        dto.setAmount(BigDecimal.TEN);

        transferService.transferAmount(dto);
        verify(accountService, times(2)).updateAccount(any(Account.class));
    }

    @Test(expected = RuntimeException.class)
    public void transferNotValidAmountTest() {
        Account from = new Account("Ivan", BigDecimal.valueOf(0), "RUR");
        from.setId(1);
        when(accountService.getAccount(1)).thenReturn(from);

        Account to = new Account("Max", BigDecimal.valueOf(1000), "RUR");
        to.setId(2);
        when(accountService.getAccount(to.getId())).thenReturn(to);

        Dto dto = new Dto();
        dto.setFrom(1);
        dto.setTo(2);
        dto.setAmount(BigDecimal.TEN);

        transferService.transferAmount(dto);
    }

    @AfterClass
    public static void stopServer() throws Exception {
        app.stopServer();
    }
}
